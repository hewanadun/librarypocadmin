<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Class for handling database queries
 */
class DBHandler 
{    
    public static function getCollections()
    {
        // Get a db connection.
        $db = JFactory::getDbo();

        // Create a new query object.
        $query = $db->getQuery(true);

        // Select all records from the collections table.
        $query
            ->select($db->quoteName(array('cl.id', 'ins.name', 'cl.endpoint')))
            ->from($db->quoteName('#__collections', 'cl'))
            ->join('INNER', $db->quoteName('#__institutes', 'ins') . ' ON ' . $db->quoteName('cl.ins_id') . ' = ' . $db->quoteName('ins.id'))
            ->order($db->quoteName('ins.name'));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);

        // Load the results
        $results = $db->loadAssocList();
        //error_log(print_r($results, TRUE));

        return $results;
    }

    public static function getCollection($cid)
    {
        // Get a db connection.
        $db = JFactory::getDbo();

        // Create a new query object.
        $query = $db->getQuery(true);

        // Select the respective record from collections table.
        $query
            ->select($db->quoteName(array('cl.id', 'ins.alias', 'ins.name', 'ins.api_url', 'cl.endpoint')))
            ->from($db->quoteName('#__collections', 'cl'))
            ->join('INNER', $db->quoteName('#__institutes', 'ins') . ' ON ' . $db->quoteName('cl.ins_id') . ' = ' . $db->quoteName('ins.id'))
            ->where($db->quoteName('cl.id') . ' = ' . $db->quote($cid));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);

        // Load the results
        $results = $db->loadAssoc();
        //error_log(print_r($results, TRUE));

        return $results;
    }


    public static function aic_artworks_AddRecords($table, $records)
    {
        // Get a db connection.
        $db = JFactory::getDbo();

        // Create and populate an object.
        /*
        id	int(11)	
        title	text	
        artist_title	varchar(128) NULL	
        artist_display	text NULL	
        place_of_origin	text NULL	
        date_display	text NULL	
        medium_display	text NULL	
        category_titles	text NULL	
        copyright_notice	text NULL	
        api_link	text NULL	
        image_id*/

        $item = new stdClass();
        foreach($records as $record){
            $item->id = $record['id'];
            $item->title = $record['title'];
            $item->artist_title = $record['artist_title'];
            $item->artist_display = $record['artist_display'];
            $item->place_of_origin = $record['place_of_origin'];
            $item->date_display = $record['date_display'];
            $item->medium_display = $record['medium_display'];
            $item->category_titles = implode(",", $record['category_titles']);
            $item->copyright_notice = $record['copyright_notice'];
            $item->api_link = $record['api_link'];
            $item->image_id = $record['image_id'];

            // Insert the object into the table.
            $result = $db->insertObject($table, $item);
            if( ! $result){
                error_log("Error occurred while inserting to table ".$table.":".$item->id);
                return false;
            }
                
        }
        
        return true;
    }

    public static function deleteAllRecords($table)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete($db->quoteName($table));

        $db->setQuery($query);

        $result = $db->execute();

        return $result;
    }
}