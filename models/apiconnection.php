<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Class for handling database queries
 */
class APIConnection 
{   
    public $id;
    public $alias;
    public $apiURL;
    public $name;
    public $endpoint;
    public $endpointURL;
    public $dataTable;

    private $result = NULL;

    public function __construct($params)
    {
        $this->id = $params['id'];
        $this->alias = $params['alias'];
        $this->name = $params['name'];
        $this->apiURL = $params['api_url'];
        $this->endpoint = $params['endpoint'];
        $this->endpointURL = $this->apiURL.$this->endpoint;
        $this->dataTable = $this->alias."_".$this->endpoint;
    }

    public function loadObjects()
    {
        global $conf;

        DBHandler::deleteAllRecords("#__".$this->dataTable);

        $insertFunc = $this->dataTable."_AddRecords";

        //eg. $url = "https://api.artic.edu/api/v1/artworks?limit=10";
        $url = $this->endpointURL.'?limit='.$conf['page-limit'];
        error_log($url);
        //TODO - modify the URL to include only necessry fields
        if($this->getData($url) && $this->result)
        {
            //error_log(print_r($this->result, true));
            $data = $this->result['data'];
            DBHandler::$insertFunc("#__".$this->dataTable, $data);

            $pageInfo = $this->result['pagination'];
            $totPages = $pageInfo['total_pages'];
            if($conf['max_pages'] >= 0 && $pageInfo['total_pages'] > $conf['max_pages']){
                $totPages = $conf['max_pages'];
            }
            $nextURL = $pageInfo['next_url'];
            $currPage = $pageInfo['current_page'];

            while($currPage < $totPages){
                $this->result = NULL;
                if($this->getData($nextURL) && $this->result){
                    $data = $this->result['data'];
                    DBHandler::$insertFunc("#__".$this->dataTable, $data);

                    $pageInfo = $this->result['pagination'];
                    $nextURL = $pageInfo['next_url'];
                    $currPage = $pageInfo['current_page'];

                    sleep(5);
                }
                else{
                    return false;
                }
                   
            }

            return true;
        }
        else
            return false;
    }

    private function getData($url)
    {	
        //send a request to API endpoint
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);

        $response = curl_exec($ch);

        if (!$response) {
            error_log("Failed to execute curl request for URL: ".$url);
            return false;
        }

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($code == 201 || $code == 200) {
            $this->result = json_decode($response, true);
            return true;
        } else {
            error_log("Error code ".$code." recived for URL: ".$url);
            return false;
        }
    }
}