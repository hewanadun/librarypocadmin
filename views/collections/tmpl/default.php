<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

Utils::addCSS('collections.css');
Utils::addJS('collections.js');

?>

<h1><?php echo $this->title; ?></h1>
<br><br>
<div>
        <legend><strong>Select a collection to load or update objects</strong></legend>
        <select class="input-large" id="collection">
                <option value="-1">-- Select Collection --</option>
            <?php foreach($this->collections as $collection){ 
                $listing = $collection['name']." - ".$collection['endpoint'];
            ?>
                <option value="<?php echo $collection['id']; ?>"><?php echo $listing; ?></option>';
            <?php }	?>	
        </select>
        <br><br>
        <button class="btn load-collection" title="Load the objects from the selected collection to the database"> Load Collection </button>
        <button class="btn update-collection" title="Update the objects of the selected collection in the database"> Update Collection </button>
        <br>
</div>
<div id="div-warning-dlg" title="Warning" style="display: none;"></div>
<div id="div-error-dlg" title="Error" style="display: none;"></div>