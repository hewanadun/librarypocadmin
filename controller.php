<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');


/**
 * Collections Component Controller
 */
class CollectionsController extends JControllerLegacy
{

    public function execute($task)
    {
        $task_func = strtolower($task) . 'Task';

        if (method_exists($this, $task_func)) {
            $this->$task_func();
            exit;
        } else {
            return parent::execute($task);
        }
    }


    public function loadTask()
    {
        $result = array('status' => 'error');

        $cid = $this->input->getInt('cid', -1);
        if ($cid == -1){
            return json_encode($result);
        }

        $collection = DBHandler::getCollection($cid);
        if (!$collection){
            return json_encode($result);
        }

        $apiCon = new APIConnection($collection);
        
        if ( ! $apiCon->loadObjects()){
            return json_encode($result);
        }

        $result['status'] = 'ok';
        return json_encode($result);
    }
}
