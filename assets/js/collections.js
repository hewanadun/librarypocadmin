jQuery(document).ready(function() {
	jQuery('.load-collection').on('click', function() {
        let cid = document.getElementById("collection").value;
        if(cid==-1)
        {
            showErrorMsg("Select a collection from the drop-down list to load objects");
            return;
        }
        
        let load_url = 'index.php?option=com_collections&task=load&cid=' + cid;
        loadCollection(load_url);
    });
});

function loadCollection(load_url){        
    jQuery.ajax({
        url: load_url,
        //data: submit_data,
        type: "POST",
        success: function(data) {
           //console.log(data);
           let message = "Successfully loaded the objects from collection";
           showInfoMsg(message);
        },
        error: function(msg){
           //console.log(msg);
           let message = "Error occured while trying to load the collection";
           showErrorMsg(message);
        }
    });
}

function showErrorMsg(msg_txt) {
    //TODO - add error dialog
	alert(msg_txt);
}

function showInfoMsg(msg_txt) {
    //TODO - add info dialog
	alert(msg_txt);
}